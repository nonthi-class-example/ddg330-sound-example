using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverSoundPlacement : MonoBehaviour
{
    [SerializeField] private Transform _riverRoot;

    private AudioListener _listener;
    private Collider[] _riverColliders;

    // Start is called before the first frame update
    void Start()
    {
        _listener = FindObjectOfType<AudioListener>();
        _riverColliders = _riverRoot.GetComponentsInChildren<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        Relocate();
    }

    private void Relocate()
    {
        float currentMinDistance = float.MaxValue;
        Vector3 closestPos = transform.position;

        foreach (Collider aCollider in _riverColliders)
        {
            Vector3 pos = aCollider.ClosestPoint(_listener.transform.position);
            float distance = Vector3.Distance(_listener.transform.position, pos);
            if (distance < currentMinDistance)
            {
                currentMinDistance = distance;
                closestPos = pos;
            }
        }

        transform.position = closestPos;
    }
}
