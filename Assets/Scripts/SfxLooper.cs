using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxLooper : MonoBehaviour
{
    [SerializeField] private SfxEventSO _sfx;
    [SerializeField] private float _interval = 1f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PlaySfxCoroutine());
    }

    private IEnumerator PlaySfxCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(_interval);
            AudioManager.Instance.PlaySfxAt(_sfx, transform.position);
        }
    }
}
