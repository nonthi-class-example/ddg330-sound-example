using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;

    public static AudioManager Instance => _instance;

    [SerializeField] private SfxSource _sfxSourcePrefab;

    private ObjectPool<SfxSource> _sfxSourcePool;

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(gameObject);

        Init();
    }

    private void Init()
    {
        _sfxSourcePool = new ObjectPool<SfxSource>(
            () =>
            {
                SfxSource sfxSource = Instantiate(_sfxSourcePrefab);
                sfxSource.transform.SetParent(transform);
                sfxSource.SetKillAction(ReleaseSfxSource);
                return sfxSource;
            },
            (source) =>
            {
                source.gameObject.SetActive(true);
            },
            (source) =>
            {
                source.gameObject.SetActive(false);
            },
            (source) =>
            {
                Destroy(source.gameObject);
            }, false);
    }

    public void PlaySfxAt(SfxEventSO sfx, Vector3 position)
    {
        SfxSource source = _sfxSourcePool.Get();
        source.Init(sfx);
        source.transform.position = position;
        source.Play();
    }

    private void ReleaseSfxSource(SfxSource source)
    {
        _sfxSourcePool.Release(source);
    }
}
