using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MyGame/Sfx")]
public class SfxEventSO : ScriptableObject
{
    [SerializeField] private AudioClip _clip;
    public AudioClip Clip => _clip;

    [SerializeField] private float _volume = 1f;
    public float Volume => _volume;
}
