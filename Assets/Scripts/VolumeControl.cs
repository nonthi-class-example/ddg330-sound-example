using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour
{
    [SerializeField] private string _volumeParam = "MasterVolume";
    [SerializeField] private AudioMixer _mixer;
    [SerializeField] private Slider _volumeSlider;
    [SerializeField] private float _volumeMult = 20f;

    private void Awake()
    {
        _mixer.GetFloat(_volumeParam, out float currentVolume);
        _volumeSlider.SetValueWithoutNotify(Mathf.Pow(10, currentVolume / _volumeMult));
        _volumeSlider.onValueChanged.AddListener(HandleSliderValueChanged);
    }

    private void HandleSliderValueChanged(float value)
    {
        _mixer.SetFloat(_volumeParam, Mathf.Log10(value) * _volumeMult);
    }

    private void OnDestroy()
    {
        _volumeSlider.onValueChanged.RemoveListener(HandleSliderValueChanged);
    }
}
