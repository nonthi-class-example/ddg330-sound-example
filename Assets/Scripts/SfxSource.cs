using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SfxSource : MonoBehaviour
{
    private AudioSource _source;
    private Action<SfxSource> _killAction;
    private bool _hasPlayed;

    private void Awake()
    {
        TryGetComponent(out _source);
    }

    private void Update()
    {
        if (_hasPlayed && !_source.isPlaying)
        {
            _killAction?.Invoke(this);
        }
    }

    public void SetKillAction(Action<SfxSource> killAction)
    {
        _killAction = killAction;
    }

    public void Init(SfxEventSO sfx)
    {
        _source.clip = sfx.Clip;
        _source.volume = sfx.Volume;
    }

    public void Play()
    {
        _source.Play();
        _hasPlayed = true;
    }
}
